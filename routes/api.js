const express = require("express");
const router = express.Router();
const LeagueController = require("../controllers/leagues");
const FixturesController = require("../controllers/fixtures");
const StandingsController = require("../controllers/standings");

/*
 * Get all fixtures from league id
 */
router.get("/v1/fixtures/:name", [
  LeagueController.getLeagueFromName,
  FixturesController.getFixturesFromLeague,
  function (req, res) {
    res.send({
      fixtures: res.locals.fixtures,
      league: res.locals.league,
      errors: res.locals.errors,
    });
  },
]);

// /*
//  * Get all fixtures from league id and selected game week
//  */
router.get("/v1/fixtures/:name/:gameWeek", [
  LeagueController.getLeagueFromName,
  FixturesController.getFixturesFromLeague,
  function (req, res) {
    res.send({
      fixtures: res.locals.fixtures,
      league: res.locals.league,
      errors: res.locals.errors,
    });
  },
]);

/*
 * Get league details and standings
 */
router.get("/v1/standings/:name", [
  LeagueController.getLeagueFromName,
  StandingsController.getLeagueStandings,
  function (req, res) {
    res.send({
      standings: res.locals.standings,
      league: res.locals.league,
      errors: res.locals.errors,
    });
  },
]);

module.exports = router;