const Leagues = require("../models/leagues");
const fixtures = require("../models/fixtures");
const standings = require("../models/standings");

/*
 * insert or update league data
 */

exports.handleLeagues = (data) => {
  const leagueData = data.api.leagues[0];
  Leagues.updateOne(
    { league_id: leagueData.league_id },
    {
      league_id: leagueData.league_id,
      name: leagueData.name,
      name_id: leagueData.name.split(" ").join("-").toLowerCase(),
      type: leagueData.type,
      country: leagueData.country,
      country_code: leagueData.country_code,
      season: leagueData.season,
      season_start: leagueData.season_start,
      season_end: leagueData.season_end,
      logo: leagueData.logo,
      flag: leagueData.flag,
      standings: leagueData.standings,
      is_current: leagueData.is_current,
    },
    { upsert: true }
  ).catch((err) => {
    console.log(err);
  });
};

/*
 * insert or update fixtures data
 */

exports.handleFixtures = (fixture) => {
  fixtures.updateOne(
    { fixture_id: fixture.fixture_id },
    fixture,
    { upsert: true },
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
};

/*
 * insert or update league standings
 */

exports.handleStandings = (standingsData, leagueId) => {
  standings.updateOne(
    { league_id: leagueId },
    {
      league_id: leagueId,
      league_table: standingsData,
    },
    { upsert: true },
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
};

/*
 * insert or update event data for a selected fixture
 */

exports.handleEvents = (events, leagueId) => {
  fixtures.updateOne(
    { fixture_id: leagueId },
    { events: events },
    { upsert: true },
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
};
