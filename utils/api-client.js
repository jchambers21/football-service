const fetch = require("node-fetch");
const keys = require("../config/keys");

module.exports = async function (url) {
  try {
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "X-RapidAPI-Host": keys.rapidAPIHost,
        "X-RapidAPI-Key": keys.rapidAPIKey,
      },
    });
    return await response.json();
  } catch (err) {
    console.log(err);
  }
};
