const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const LeagueSchema = new Schema({
  league_id: {
    type: Number,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  name_id: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  country_code: {
    type: String,
    required: true,
  },
  season: {
    type: Number,
    required: true,
  },
  season_start: {
    type: Date,
    required: true,
  },
  season_end: {
    type: Date,
    required: true,
  },
  logo: {
    type: String,
    required: true,
  },
  flag: {
    type: String,
    required: true,
  },
  standings: {
    type: Number,
    required: true,
  },
  is_current: {
    type: Number,
    required: true,
  },
  disorder: {
    type: Number
  }
});

module.exports = Leagues = mongoose.model("leagues", LeagueSchema);
