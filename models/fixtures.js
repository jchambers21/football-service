const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FixturesSchema = new Schema({
  fixture_id: {
    type: Number,
    required: true,
    unique : true
  },
  league_id: {
    type: Number,
    required: true
  },
  league: {
    type: Object,
  },
  event_date: {
    type: Date,
    required: true
  },
  event_timestamp: {
   type: Number
  },
  firstHalfStart: {
    type: Number
  },
  secondHalfStart: {
    type: Number
  },
  round: {
    type: String
  },
  status: {
    type: String
  },
  statusShort: {
    type: String
  },
  elapsed: {
    type: Number
  },
  venue: {
    type: String
  },
  referee: {
    type: String
  },
  homeTeam: {
    type: Object
  },
  awayTeam: {
    type: Object
  },
  goalsHomeTeam: {
    type: Number
  },
  goalsAwayTeam: {
    type: Number
  },
  score: {
    type: Object
  },
  events: {
    type: Array
  }
});

module.exports = Fixtures = mongoose.model("fixtures", FixturesSchema);
