const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StandingsSchema = new Schema({
  league_id: {
    type: Number,
    required: true,
    unique : true
  },
  league_table: {
    type: Array,
    required: true,
  }
});

module.exports = Standings = mongoose.model("standings", StandingsSchema);
