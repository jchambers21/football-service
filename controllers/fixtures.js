const Fixtures = require("../models/fixtures");

exports.getFixturesFromLeague = (req, res, next) => {
  if (res.locals.errors > 0 || !res.locals.league) {
    res.locals.fixtures = [];
    next();
  }

  const findParamter = req.params.gameWeek
    ? {
        league_id: res.locals.league.league_id,
        round: { $regex: `.*${req.params.gameWeek}.*` },
      }
    : { league_id: res.locals.league.league_id };

  Fixtures.find(findParamter, {
    fixture_id: 1,
    round: 1,
    statusShort: 1,
    elapsed: 1,
    homeTeam: 1,
    awayTeam: 1,
    goalsHomeTeam: 1,
    goalsAwayTeam: 1,
    score: 1,
    event_date: 1,
  })
    .then((response) => {
      if (response.length <= 0) {
        res.locals.errors = ["NO_FIXTURES_AVAILABLE"];
        res.locals.fixtures = [];
        next();
      }
      res.locals.fixtures = response;
      res.locals.errors = [];
      next();
    })
    .catch((err) => {
      res.locals.errors = ["NO_FIXTURES_AVAILABLE", err];
      res.locals.fixtures = [];
    });
};
