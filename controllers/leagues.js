const Leagues = require("../models/leagues");

/*
* Get league from formatted name E.g "premier-league"
*/
exports.getLeagueFromName = (req, res, next) => {
  Leagues.findOne({ name_id: req.params.name })
    .then((response) => {
      res.locals.errors = response ? [] : ["INVAILD_LEAGUE_ID"];
      res.locals.league = response;
      next();
    })
    .catch((err) => {
      res.locals.errors = [err];
      res.locals.leagueId = [];
      next();
    });
};

