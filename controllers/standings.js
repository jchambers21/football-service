const Standings = require("../models/standings");

exports.getLeagueStandings = (req, res, next) => {
  if (res.locals.errors > 0 || !res.locals.league) {
    res.locals.standings = [];
    next();
  }
  Standings.find({ league_id: res.locals.league.league_id })
    .then((response) => {
      if (response.length <= 0) {
        res.locals.errors = ["NO_LEAGUE_STANDINGS_AVAILABLE"];
        res.locals.standings = [];
        next();
      }
      res.locals.errors = [];
      res.locals.standings = response[0].league_table;
      next();
    })
    .catch((err) => {
      res.locals.errors = ["NO_FIXTURES_AVAILABLE", err];
      res.locals.fixtures = [];
      next();
    });
};
