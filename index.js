const express = require("express");
const bodyParser = require("body-parser");
const port = 3000;
const mongoose = require("mongoose");
const app = express();
const CronJob = require("cron").CronJob;
const apiClient = require("./utils/api-client");
const keys = require("./config/keys");
const cronTasks = require("./utils/cron-tasks");
const standings = require("./models/standings");

app.use(bodyParser.json());

mongoose
  .connect(keys.mongoURI, { useNewUrlParser: true })
  .then(() => console.log("MongoDB successfully connected"))
  .catch((err) => console.log(err));

const leagueIds = [
  2790, // prem
  2794, // champ
  2803, // league 1
  2796, // league 2
  2725, // Community Sheild
  2781, // League cup
  2791, // Fa cup
  2792, // EFL trophy
  2664, // ligue 1 - france
  2652, // league 2
  2755, // Bundesliga - germany
  2743, // Bundesliga 2
  2677, // DFB Pokal
  2751, // Super Cup
  403, // Euro Champ - world
  1422, // UEFA Nations League
  2792, // Europa
  2771, // UCL
  1324, // Friendlies
  1264, // MLS - USA
];

const fetchLeagues = new CronJob("0 1 * * *", function () {
  leagueIds.map((id) => {
    apiClient(`${keys.apiUrl}/v2/leagues/league/${id}`).then((data) =>
      cronTasks.handleLeagues(data)
    );
  });
});

const fetchFixtures = new CronJob("0 * * * *", function () {
  leagueIds.map((id) => {
    apiClient(
      `${keys.apiUrl}/v2/fixtures/league/${id}?timezone=Europe/London`
    ).then((data) => {
      if (data.api.results > 0) {
        data.api.fixtures.forEach((fixture) => {
          cronTasks.handleFixtures(fixture);
        });
      }
    });
  });
});

const fetchStandings = new CronJob("0 * * * *", function () {
  leagueIds.map((id) => {
    apiClient(`${keys.apiUrl}/v2/leagueTable/${id}`).then((data) => {
      if (data.api.results > 0) {
        cronTasks.handleStandings(data.api.standings, id);
      }
    });
  });
});

// only use when back dating data
const fetchEvents = () => {
  apiClient(`${keys.apiUrl}/v2/events/588624`).then((data) => {
    if (data.api.results > 0) {
      cronTasks.handleEvents(data.api.events, 588624);
    }
  });
};

const fetchLiveFixtures = new CronJob("* * * * *", function () {
  const test = leagueIds.join("-");
  apiClient(
    `${keys.apiUrl}/v2/fixtures/live/${test}?timezone=Europe/London`
  ).then((data) => {
    if (data.api.results > 0) {
      data.api.fixtures.forEach((fixture) => {
        cronTasks.handleFixtures(fixture);
      });
    }
  });
});

app.get("/", (req, res) => {
  standings
    .find()
    .then((response) => {
      res.status(200).json({ response });
    })
    .catch((err) => {
      res.status(404).json({ err });
    });
});

fetchLeagues.start();
fetchFixtures.start();
fetchStandings.start();
// fetchEvents();
fetchLiveFixtures.start();

console.log(`Football-mirco service listening on port ${port}`);
app.listen(port);
